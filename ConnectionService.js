/**
 * kermit - soa infrastructure for node js
 *
 * @package     kermit-mongoose
 * @copyright   Copyright (c) 2016, Alrik Zachert
 * @license     https://gitlab.com/kermit-js/kermit-mongoose/blob/master/LICENSE BSD-2-Clause
 */

'use strict';

const
  mongoose = require('mongoose'),
  Service = require('kermit/Service');

/**
 * The kermit mongoose connection wrapper class.
 */
class ConnectionService extends Service {
  /**
   * Try to load default config from environment variables.
   *
   * @returns {Object}
   */
  getDefaultServiceConfig() {
    let
      env = process.env,
      config = {},
      envMapping = {
        MONGODB_HOST: 'host',
        MONGODB_PORT: 'port',
        MONGODB_USER: 'username',
        MONGODB_PASS: 'password',
        MONGODB_DATABASE: 'database'
      };

    for (let envName in envMapping) {
      if (env[envName]) {
        config[envMapping[envName]] = env[envName];
      }
    }

    return {
      connection: config,
      promiseLibrary: Promise
    };
  }

  /**
   * Bootstrap the mongoose connection and register event listeners.
   *
   * @returns {ConnectionService}
   */
  bootstrap() {
    let promiseLibrary = this.serviceConfig.get('promiseLibrary');

    if (promiseLibrary) {
      mongoose.Promise = promiseLibrary;
    }

    this.connection = mongoose.createConnection();

    this.on('connected', () => this.emit('ready'));

    return this;
  }

  /**
   * Open the connection to the mongodb.
   *
   * @returns {ConnectionService}
   */
  launch() {
    this.connection.open(
      this._compileConnectionString(),
      this._getConnectionOptions()
    );

    return this;
  }

  /**
   * Return the mongoose singleton.
   *
   * @returns {mongoose}
   */
  getMongoose() {
    return mongoose;
  }

  /**
   * Return the managed mongoose connection.
   *
   * @returns {Connection}
   */
  getConnection() {
    return this.connection;
  }

  /**
   * Return the configured connection options if any.
   *
   * @returns {*}
   * @private
   */
  _getConnectionOptions() {
    return this.serviceConfig.get('options');
  }

  /**
   * Build the mongodb connection uri based on the given config.
   *
   * @returns {string}
   * @private
   */
  _compileConnectionString() {
    let
      params = this.serviceConfig.get('connection', {}),
      uri = 'mongodb://';

    if (params.user) {
      uri += encodeURIComponent(params.username);

      if (params.password) {
        uri += `:${encodeURIComponent(params.password)}@`;
      } else {
        uri += '@';
      }
    }

    uri += `${params.host}:${params.port}/${params.database}`;

    return uri;
  }

  /**
   * Proxy mongoose connection event subscriptions to the connection instance.
   *
   * @param {String} event
   * @param {Function} callback
   * @returns {ConnectionService}
   */
  on(event, callback) {
    if ([
      'connecting', 'connected', 'open', 'disconnecting', 'disconnected', 'close',
      'reconnected', 'error', 'fullsetup', 'all'
    ].indexOf(event) !== -1) {
      this.connection.on(event, callback);
    } else {
      super.on(event, callback);
    }

    return this;
  }
}

module.exports = ConnectionService;
